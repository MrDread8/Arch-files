#!/bin/bash
command="$(cat /sys/class/power_supply/BAT1/capacity)"
status="$(cat /sys/class/power_supply/BAT1/status)"
if [ "$status" = "Charging" ]; then
    case $command in
            100|9[0-9])      icon="";;
            8[0-9]|7[0-9])   icon="";;
            6[0-9]|5[0-9])   icon="";;
            4[0-9]|3[0-9])   icon="";;
            *)               icon="";;
    esac
elif [ "$status" = "Discharging" ]; then
    case $command in
            100|9[0-9])      icon="";;
            8[0-9]|7[0-9])   icon="";;
            6[0-9]|5[0-9])   icon="";;
            4[0-9]|3[0-9])   icon="";;
            *)               icon="";;
    esac
else
    icon=""
fi
printf " %s %s \\n" "$icon" "$command"
