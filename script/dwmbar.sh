#!/bin/bash

while true; do
    exec $HOME/Arch-files/script/dwm/weather.sh
    sleep 9000s
done &

while true; do 
    exec $HOME/Arch-files/script/dwm/update.sh
   sleep 18000s 
done &

while true; do
    WEATHER=$(<$HOME/Arch-files/script/dwm/weather.txt)
    CPU=$($HOME/Arch-files/script/dwm/cpu.sh)
    UPDATE=$(<$HOME/Arch-files/script/dwm/update.txt)
    MEM=$($HOME/Arch-files/script/dwm/mem.sh)
    NET=$($HOME/Arch-files/script/dwm/network.sh)
    BAT=$($HOME/Arch-files/script/dwm/battery.sh)
    TIME=$($HOME/Arch-files/script/dwm/time.sh)

    xsetroot -name "| $WEATHER | $UPDATE | $NET | $MEM | $CPU | $BAT | $TIME"
    sleep 5s
done &
